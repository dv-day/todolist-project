import React from "react";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import Home from "./component/Home";
import TodoList from "./component/TodoList";
import Albums from "./component/Albums";
import Gallary from "./component/Gallary";
import Login from "./component/Login";
import PrivateRoute from "./PrivateRoute";
import { login, logout } from "./reducers/loginReducer";
import { useDispatch } from "react-redux";
import Logout from "./component/Logout";

const MainRoute = () => {
  // const isLogin = useSelector(state => state.isLogin);
  const dispatch = useDispatch();
  return (
    <BrowserRouter>
      <Route
        path="/processLogin"
        render={() => {
          // alert("welcome");
          dispatch(login());
          // localStorage.setItem("isLogin", true);
          return <Redirect to="/users/" />;
        }}
      ></Route>

      <Route
        path="/processLogout"
        render={() => {
          // alert("bye");
          dispatch(logout());
          // localStorage.setItem("isLogin", false);
          return <Redirect to="/login" />;
        }}
      ></Route>
      <Route exact path="/" component={Home}></Route>
      <Route path="/login" component={Login}></Route>
      <Route path="/logout" component={Logout}></Route>

      {/* <PrivateRoute exact path="/users/" component={Home}></PrivateRoute> */}
      <PrivateRoute
        path="/users/:user_id/todo"
        component={TodoList}
      ></PrivateRoute>
      <PrivateRoute
        exact
        path="/users/:user_id/albums"
        component={Albums}
      ></PrivateRoute>
      <PrivateRoute
        path="/users/:user_id/albums/:album_id"
        component={Gallary}
      ></PrivateRoute>
      <Route exact path="/users/" component={Home}></Route>
      {/* <Route path="/users/:user_id/todo" component={TodoList}></Route> */}
      {/* <Route exact path="/users/:user_id/albums" component={Albums}></Route> */}
      {/* <Route path="/users/:user_id/albums/:album_id" component={Gallary}></Route> */}
    </BrowserRouter>
  );
};
export default MainRoute;
