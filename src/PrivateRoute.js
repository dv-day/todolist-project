import React from "react";
import { Route, Redirect } from "react-router-dom";
// import { useSelector } from "react-redux";

const PrivateRoute = ({ path, component: Component }) => {
  // const isLoginState = useSelector(state => state.isLogin);
  return (
    <Route
      path={path}
      render={props => {
        const isLogin = localStorage.getItem("isLogin", false);
        console.log(isLogin)
        return isLogin === "true" ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        );
      }}
    ></Route>
  );
};

export default PrivateRoute;
