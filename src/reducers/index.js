import { combineReducers } from "redux";
import todoReducer from "./todoReducer";
import { userReducer } from "./userReducer";
import navReducer from "./navReducer";
import loginReducer from "./loginReducer"

export const rootReducer = combineReducers({
  allTodos: todoReducer,
  allUsers: userReducer,
  nav: navReducer,
  isLoginState: loginReducer
});
