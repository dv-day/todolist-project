const LOGIN = "LOGIN";
const LOGOUT = "LOGOUT";

const onlineState = {
  isLoginState: false
};

export const login = () => {
  alert("logging in");
  return {
    type: LOGIN
  };
};

export const logout = () => {
  return {
    type: LOGOUT
  };
};

const loginReducer = (state = onlineState, action) => {
  switch (action.type) {
    case LOGIN:
      localStorage.setItem("isLogin", true);
      return {
        ...state,
        isLoginState: true
      };
    case LOGOUT:
      localStorage.setItem("isLogin", false);
      return {
        ...state,
        isLoginState: false
      };
    default:
      return state;
  }
};

export default loginReducer;
