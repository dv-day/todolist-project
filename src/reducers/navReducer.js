const CHANGE_COLLAPSE = "CHANGE_COLLAPSE";

const collapsedState = {
  collapsed: false
};

export const setCollapsed = collapsed => {
  return {
    type: CHANGE_COLLAPSE,
    collapsed: collapsed
  };
};

export const navReducer = (state = collapsedState, action) => {
  switch (action.type) {
    case CHANGE_COLLAPSE:
        console.log(state.collapsed)
      return state.collapsed === true
        ? {
            collapsed: false
          }
        : {
            collapsed: true
          };
    default:
      return state;
  }
};

export default navReducer;
