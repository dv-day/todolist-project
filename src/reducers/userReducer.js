const FETCH_USERS_BEGIN = "FETCH_USERS_BEGIN";
const FETCH_USERS_DONE = "FETCH_USERS_DONE";
const FETCH_ONE_USERS_DONE = "FETCH_ONE_USERS_DONE";
const FETCH_USERS_ERROR = "FETCH_USERS_ERROR";
const ON_SEARCH = "ON_SEARCH";

const initialState = {
  users: [],
  loading: false,
  error: "",
  keyword: ""
};
export const searchUser = value => {
  return {
    type: ON_SEARCH,
    keyword: value
  };
};

export const fetchAllUsers = () => {
  return dispatch => {
    dispatch(fetchAllUsersBegin());
    return fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(data => {
        dispatch(fetchAllUsersDone(data));
      })
      .catch(error => dispatch(fetchAllUsersError(error)));
  };
};

export const fetchUser = userId => {
  return dispatch => {
    dispatch(fetchAllUsersBegin());
    return fetch("https://jsonplaceholder.typicode.com/users/" + userId)
      .then(res => res.json())
      .then(data => {
        dispatch(fetchOneUserDone(data));
      })
      .catch(error => dispatch(fetchAllUsersError(error)));
  };
};

export const fetchAllUsersBegin = () => {
  return {
    type: FETCH_USERS_BEGIN
  };
};

export const fetchAllUsersDone = users => {
  return {
    type: FETCH_USERS_DONE,
    payload: users
  };
};

export const fetchOneUserDone = users => {
  return {
    type: FETCH_ONE_USERS_DONE,
    payload: users
  };
};
export const fetchAllUsersError = error => {
  return {
    type: FETCH_USERS_ERROR,
    payload: error
  };
};
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_USERS_DONE:
      return {
        ...state,
        users: action.payload,
        loading: false,
        error: ""
      };
    case FETCH_ONE_USERS_DONE:
      console.log(action.payload);
      return {
        ...state,
        users: action.payload,
        loading: false,
        error: ""
      };
    case FETCH_USERS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case ON_SEARCH:
      return {
        ...state,
        keyword: action.keyword
      };
    default:
      return state;
  }
};
