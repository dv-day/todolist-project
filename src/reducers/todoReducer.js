const FETCH_TODOS_BEGIN = "FETCH_TODOS_BEGIN";
const FETCH_TODOS_DONE = "FETCH_TODOS_DONE";
const FETCH_TODOS_ERROR = "FETCH_TODOS_ERROR";
const DONE_TODO = "DONE_TODO";
const FILTER_ALL = "FILTER_ALL";
const FILTER_DONE = "FILTER_DONE";
const FILTER_NOT_DONE = "FILTER_NOT_DONE";
const SEARCH_BY_KEYWORD = "SEARCH_BY_KEYWORD";

const initialUser = {
  todos: [],
  loading: false,
  error: "",
  show: "all",
  keyword: ""
};

export const fetchTodos = userId => {
  return dispatch => {
    dispatch(fetchAllTodosBegin());
    return fetch("https://jsonplaceholder.typicode.com/todos?userId=" + userId)
      .then(response => response.json())
      .then(data => {
        dispatch(fetchAllTodosDone(data));
      })
      .catch(error => dispatch(fetchAllUsersError(error)));
  };
};

export const filterTodos = value => {
  if (value === "done") {
    return { type: FILTER_DONE };
  } else if (value === "notdone") {
    return { type: FILTER_NOT_DONE };
  } else {
    return { type: FILTER_ALL };
  }
};
export const fetchAllTodosBegin = () => {
  return {
    type: FETCH_TODOS_BEGIN
  };
};

export const fetchAllTodosDone = todos => {
  return {
    type: FETCH_TODOS_DONE,
    payload: todos
  };
};

export const fetchAllUsersError = error => {
  return {
    type: FETCH_TODOS_ERROR,
    payload: error
  };
};

export const doneTodo = index => {
  return {
    type: DONE_TODO,
    // payload: todos,
    index: index,
    status: true
  };
};

export const onSearch = keyword => {
  return {
    type: SEARCH_BY_KEYWORD,
    keyword: keyword
  };
};
const todoReducer = (state = initialUser, action) => {
  switch (action.type) {
    case FETCH_TODOS_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_TODOS_DONE:
      return {
        ...state,
        todos: action.payload,
        loading: false,
        error: ""
      };

    case FETCH_TODOS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case DONE_TODO:
      console.log(state.todos);
      return {
        ...state,
        todos: state.todos.map((todos, i) =>
          i === action.index - 1
            ? {
                ...todos,
                completed: action.status
              }
            : todos
        )
      };
    case FILTER_DONE:
      return {
        ...state,
        show: "done"
      };

    case FILTER_NOT_DONE:
      return {
        ...state,
        show: "notdone"
      };
    case FILTER_ALL:
      return {
        ...state,
        show: "all"
      };
    case SEARCH_BY_KEYWORD:
      return {
        ...state,
        keyword: action.keyword
      };
    default:
      return state;
  }
};

export default todoReducer;
