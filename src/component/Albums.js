import React, { useState, useEffect } from "react";
import { List, Layout, Icon, PageHeader, Typography } from "antd";
// import { Link } from "react-router-dom";

const { Header, Content } = Layout;
const { Paragraph } = Typography;

const Albums = props => {
  const userId = props.match.params.user_id;

  const [albums, setAlbums] = useState([]);
  const [users, setUsers] = useState([]);

  const fetchUser = () => {
    fetch("https://jsonplaceholder.typicode.com/users/" + userId)
      .then(response => response.json())
      .then(data => {
        setUsers(data);
        console.log(data);
      })
      .catch(error => console.log(error));
  };

  const fetchAlbums = () => {
    fetch("https://jsonplaceholder.typicode.com/albums?userId=" + userId)
      .then(response => response.json())
      .then(data => {
        setAlbums(data);
        console.log(data);
      })
      .catch(error => console.log(error));
  };

  useEffect(() => {
    fetchAlbums();
    fetchUser();
  }, []);

  const content = (
    <div className="content">
      <Paragraph>
        <Icon type="mail" />
        Email : {users.email}
      </Paragraph>
      <Paragraph>
        <Icon type="ie" /> Website : {users.website}
      </Paragraph>
      <Paragraph>
        <Icon type="phone" /> Telephone : {users.phone}
      </Paragraph>
    </div>
  );
  const routes = [
    {
      path: "index",
      breadcrumbName: users.name
    },
    {
      path: "first",
      breadcrumbName: "Albums"
    }
  ];
  return (
    <div>
      <Header style={{ background: "#fff", padding: 0 }}>
        <PageHeader
          title={users.name}
          style={{
            border: "1px solid rgb(235, 237, 240)"
          }}
          subTitle={users.username}
          avatar={{
            src:
              "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          }}
          breadcrumb={{ routes }}
        >
          <Content
            extraContent={
              <img
                src="https://gw.alipayobjects.com/mdn/mpaas_user/afts/img/A*KsfVQbuLRlYAAAAAAAAAAABjAQAAAQ/original"
                alt="content"
              />
            }
          >
            {content}
          </Content>
        </PageHeader>
      </Header>
      <Content style={{ margin: "10% 16px" }}>
        <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <List
            size="large"
            header={<div>Albums</div>}
            bordered
            dataSource={albums}
            renderItem={item => (
              <List.Item>
                <a href={"/users/" + item.userId + "/albums/" + item.id}>
                  {item.title}
                </a>
              </List.Item>
            )}
          />
        </div>
      </Content>
    </div>
  );
};
export default Albums;
