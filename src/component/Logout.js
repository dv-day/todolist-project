import React from "react";
import { Layout, Form, Button } from "antd";
const { Header, Content } = Layout;

const Logout = () => {
  return (
    <div>
      <Header
        style={{
          background: "#fff",
          padding: 0,
          textAlign: "center",
          fontSize: "35px"
        }}
      >
        <h1>LOGIN</h1>
      </Header>
      <Content style={{ margin: "15% 16px" }}>
        <div
          style={{
            width: "30%",
            height: "350px",
            backgroundColor: "#fff",
            padding: "5%",
            textAlign: "center",
            display: "block",
            margin: "10% auto"
          }}
        >
          <Form>
            <h1>You're currently logged in</h1>
            <Button
              size="large"
              type="primary"
              style={{ width: "100%", marginTop: 50 }}
              onClick={() => (window.location = "/processLogout")}
            >
              Log out
            </Button>
          </Form>
        </div>
      </Content>
    </div>
  );
};

export default Logout;
