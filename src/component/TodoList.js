import React, { useEffect } from "react";
import {
  List,
  Card,
  Layout,
  Icon,
  Typography,
  PageHeader,
  Button,
  Select,
  Tag,
  Row,
  Col,
  Badge,
  Input
} from "antd";
import { useSelector, useDispatch } from "react-redux";
import { fetchUser } from "../reducers/userReducer";
import {
  fetchTodos,
  doneTodo,
  filterTodos,
  onSearch
} from "../reducers/todoReducer";

const { Paragraph } = Typography;
const { Option } = Select;
const { Header, Content } = Layout;
const { Search } = Input;

const TodoList = props => {
  const users = useSelector(state => state.allUsers.users);
  const showFilter = useSelector(state => state.allTodos.show);
  const keyword = useSelector(state => state.allTodos.keyword);
  const todoLists = useSelector(state => state.allTodos.todos);

  const userId = props.match.params.user_id;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUser(userId));
    dispatch(fetchTodos(userId));
  }, []);

  const content = (
    <div className="content">
      <Paragraph>
        <Icon type="mail" />
        Email : {users.email}
      </Paragraph>
      <Paragraph>
        <Icon type="ie" /> Website : {users.website}
      </Paragraph>
      <Paragraph>
        <Icon type="phone" /> Telephone : {users.phone}
      </Paragraph>
    </div>
  );

  const routes = [
    {
      path: "index",
      breadcrumbName: users.name
    },
    {
      path: "first",
      breadcrumbName: "To Do Lists"
    }
  ];

  const handleChange = value => {
    dispatch(filterTodos(value));
  };

  var renderItems = todoLists;
  if (showFilter === "done") {
    renderItems = renderItems.filter(todo => {
      return todo.completed === true;
    });
  } else if (showFilter === "notdone") {
    renderItems = renderItems.filter(todo => {
      return todo.completed === false;
    });
  }

  if (keyword !== "") {
    renderItems = renderItems.filter(todo => {
      
      console.log(keyword);
      return todo.title.toLowerCase().match(keyword.toLowerCase());
    });
  }

  return (
    <div>
      <Header style={{ background: "#fff", padding: 0 }}>
        <PageHeader
          title={users.name}
          style={{
            border: "1px solid rgb(235, 237, 240)"
          }}
          subTitle={users.username}
          avatar={{
            src:
              "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          }}
          breadcrumb={{ routes }}
        >
          <Content
            extraContent={
              <img
                src="https://gw.alipayobjects.com/mdn/mpaas_user/afts/img/A*KsfVQbuLRlYAAAAAAAAAAABjAQAAAQ/original"
                alt="content"
              />
            }
          >
            {content}
          </Content>
        </PageHeader>
      </Header>
      <Content style={{ margin: "5% 16px" }}>
        <div
          style={{
            padding: 24,
            background: "#fff",
            minHeight: 360,
            display: "block"
          }}
        >
          <Row style={{ marginTop: "6%" }}>
            
            <Col style={{ float: "right" }}>
              <Badge count={renderItems.length}>
              <Search
                size="large"
                placeholder="Search"
                onChange={e => dispatch(onSearch(e.target.value))}
                style={{ width: "350px" }}
              />
               </Badge>
            </Col>
           
            <Col style={{ float: "right" }}>
              
                <Select
                  size="large"
                  defaultValue="All Activities"
                  style={{ width: 150 }}
                  onChange={handleChange}
                >
                  <Option value="all">All</Option>
                  <Option value="done">DONE</Option>
                  <Option value="notdone">NOT DONE</Option>
                </Select>
              
            </Col>
          </Row>

          <List
            style={{ margin: "20px" }}
            grid={{
              gutter: 1,
              xs: 1,
              sm: 1,
              md: 1,
              lg: 1,
              xl: 1,
              xxl: 1
            }}
            dataSource={renderItems}
            renderItem={(item, index) => (
              <List.Item>
                <Card title={item.title}>
                  {item.completed ? (
                    <Tag color="green">DONE</Tag>
                  ) : (
                    <Tag color="red">NOT DONE</Tag>
                  )}
                  {item.completed ? null : (
                    <Button
                      type="primary"
                      style={{ float: "right" }}
                      onClick={() => {
                        dispatch(doneTodo(item.id));
                        alert("Good job! Carry on");
                      }}
                    >
                      <Icon type="check" />
                      Done
                    </Button>
                  )}
                </Card>
              </List.Item>
            )}
          />
        </div>
      </Content>
    </div>
  );
};

export default TodoList;
