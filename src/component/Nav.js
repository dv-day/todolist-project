import React from "react";
import { Layout, Menu, Icon, BackTop } from "antd";
import MainRoute from "../MainRoute";
import { useSelector, useDispatch } from "react-redux";
import { setCollapsed } from "../reducers/navReducer";

const { Footer, Sider } = Layout;

const Nav = () => {
  const collapsed = useSelector(state => state.nav.collapsed);
  // const isLogin = useSelector(state => state.isLoginState);
  const dispatch = useDispatch();

  const isLogin = localStorage.getItem("isLogin");
  //   const [collapsed, setCollapsed] = useState(true);

  const onCollapse = collapsed => {
    dispatch(setCollapsed(collapsed));
  };

  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
          <div className="logo" />
          <Menu theme="light" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item key="1">
              <Icon type="team" />
              <span>User Lists</span>
              <a href="/users">.</a>
            </Menu.Item>
            {isLogin == "true" ? (
              <Menu.Item key="2">
                <Icon type="logout" />
                <span>Log out</span>
                <a href="/logout">.</a>
              </Menu.Item>
            ) : (
              <Menu.Item key="3">
                <Icon type="login" />
                <span>Log in</span>
                <a href="/login">.</a>
              </Menu.Item>
            )}
          </Menu>
        </Sider>
        <Layout>
          <MainRoute />
          <div>
            <BackTop>
              <div
                className="ant-back-top-inner"
                style={{ border: "2px solid gray", textAlign: "center" }}
              >
                <Icon type="to-top" style={{ fontSize: "23px" }} />
                TOP
              </div>
            </BackTop>
          </div>
          <Footer style={{ textAlign: "center" }}>
            created by Supreme5678
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
};

export default Nav;
