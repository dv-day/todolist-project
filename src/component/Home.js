import React, { useEffect } from "react";
import { Input, List, Avatar, Skeleton, Layout, Row, Col, Badge } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { fetchAllUsers, searchUser } from "../reducers/userReducer";

const { Header, Content } = Layout;
const { Search } = Input;

const Home = () => {
  const users = useSelector(state => state.allUsers.users);
  const keyword = useSelector(state => state.allUsers.keyword);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllUsers());
  }, [dispatch]);

  var userTempList = users;

  if (keyword !== "") {
    userTempList = userTempList.filter(kw => {
      return kw.name.toLowerCase().match(keyword.toLowerCase());
    });
  }

  return (
    <div>
      <Header style={{ background: "#fff", padding: 0 }}>
        <h1 style={{ textAlign: "center", fontSize: "35px" }}>TO DO APP</h1>
      </Header>
      <Content style={{ margin: "5% 16px" }}>
        <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <Row>
            <Col
              style={{
                // margin: "10px 40px",
                width: "250px",
                float: "right"
              }}
            >
              <Badge count={userTempList.length}>
                <Search
                  placeholder="search"
                  size="large"
                  onChange={e => dispatch(searchUser(e.target.value))}
                />
              </Badge>
            </Col>
          </Row>
          <List
            className="demo-loadmore-list"
            loading={users.loading}
            itemLayout="horizontal"
            // loadMore={loadMore}
            dataSource={userTempList}
            renderItem={item => (
              <List.Item
                actions={[
                  <a
                    key="list-loadmore-edit"
                    href={"/users/" + item.id + "/todo"}
                  >
                    To Do
                  </a>,
                  <a
                    key="list-loadmore-more"
                    href={"/users/" + item.id + "/albums"}
                  >
                    Albums
                  </a>
                ]}
              >
                <Skeleton avatar title={false} loading={item.loading} active>
                  <List.Item.Meta
                    avatar={
                      <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    }
                    title={item.name}
                    description=""
                  />
                  <div>{item.username}</div>
                </Skeleton>
              </List.Item>
            )}
          />
        </div>
      </Content>
    </div>
  );
};
export default Home;
