import React from "react";
import { Layout, Form, Input, Button, Icon } from "antd";
// import { useSelector, useDispatch } from "react-redux";
// import { login, logout } from "../reducers/loginReducer";

const { Header, Content } = Layout;

const Login = () => {
  // const isLogin = useSelector(state => state.isLogin);
  // const dispatch = useDispatch();

  // var tmpUsername, tmpPassword;
  // const getUsername = value => {
  //   tmpUsername = value;
  // };
  // const getPassword = value => {
  //   tmpPassword = value;
  // };
  return (
    <div>
      <Header
        style={{
          background: "#fff",
          padding: 0,
          textAlign: "center",
          fontSize: "35px"
        }}
      >
        <h1>LOGIN</h1>
      </Header>
      <Content style={{ margin: "5% 16px" }}>
        <div
          style={{
            width: "30%",
            height: "350px",
            backgroundColor: "#fff",
            padding: "5%",
            textAlign: "center",
            display: "block",
            margin: "10% auto"
          }}
        >
          <Form>
            <Input
              prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
              type="text"
              placeholder="Username"
              name="username"
              // onChange={e => getUsername(e.target.value)}
              size="large"
            ></Input>
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              type="password"
              placeholder="Password"
              name="psw"
              // onChange={e2 => getPassword(e2.target.value)}
              size="large"
              style={{ margin: "15px auto" }}
            ></Input>
            <Button
              size="large"
              type="primary"
              style={{ width: "100%" }}
              onClick={() => window.location="/processLogin"}
            >
              Login
            </Button>
          </Form>
          <div style={{ fontSize: "20px", margin: "15px auto" }}>
            <Icon type="facebook" style={{marginRight:"10px"}} />
            <Icon type="google" />
          </div>
        </div>
      </Content>
    </div>
  );
};

export default Login;
