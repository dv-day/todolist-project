import React, { useState, useEffect } from "react";
import { List, Layout, Icon, PageHeader, Typography, Card } from "antd";

const { Header, Content} = Layout;
const { Paragraph } = Typography;
const { Meta } = Card;
const Gallary = props => {
  const userId = props.match.params.user_id;
  const album_id = props.match.params.album_id;

  const [albums, setAlbums] = useState([]);
  const [users, setUsers] = useState([]);

  const fetchUser = () => {
    fetch("https://jsonplaceholder.typicode.com/users/" + userId)
      .then(response => response.json())
      .then(data => {
        setUsers(data);
        console.log(data);
      })
      .catch(error => console.log(error));
  };

  const fetchAlbums = () => {
    fetch("https://jsonplaceholder.typicode.com/photos?albumId=" + album_id)
      .then(response => response.json())
      .then(data => {
        setAlbums(data);
        console.log(data);
      })
      .catch(error => console.log(error));
  };

  useEffect(() => {
    fetchAlbums();
    fetchUser();
  }, []);

  const content = (
    <div className="content">
      <Paragraph>
        <Icon type="mail" />
        Email : {users.email}
      </Paragraph>
      <Paragraph>
        <Icon type="ie" /> Website : {users.website}
      </Paragraph>
      <Paragraph>
        <Icon type="phone" /> Telephone : {users.phone}
      </Paragraph>
    </div>
  );
  const routes = [
    {
      path: "index",
      breadcrumbName: users.name
    },
    {
      path: "first",
      breadcrumbName: "Albums"
    }
  ];
  return (
    <div>
      <Header style={{ background: "#fff", padding: 0 }}>
        <h1 style={{ textAlign: "center", fontSize: "35px" }}>Gallary</h1>
        <PageHeader
          title={users.name}
          style={{
            border: "1px solid rgb(235, 237, 240)"
          }}
          subTitle={users.username}
          avatar={{
            src:
              "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          }}
          breadcrumb={{ routes }}
        >
          <Content
            extraContent={
              <img
                src="https://gw.alipayobjects.com/mdn/mpaas_user/afts/img/A*KsfVQbuLRlYAAAAAAAAAAABjAQAAAQ/original"
                alt="content"
              />
            }
          >
            {content}
          </Content>
        </PageHeader>
      </Header>
      <Content style={{ margin: "12% 16px" }}>
        <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={albums}
            renderItem={item => (
              <List.Item>
                <Card
                  hoverable
                  style={{ width: 240 }}
                  cover={<img alt={item.title} src={item.url} />}
                >
                  <Meta title={item.title} description="www.instagram.com" />
                </Card>
              </List.Item>
            )}
          />
        </div>
      </Content>
    </div>
  );
};
export default Gallary;
